#include <iostream>
#include <iomanip>

int
main()
{
    int workHours = 0;
    while (workHours != -1) {
        std::cout << "Enter hours worked (-1 to end): ";
        std::cin >> workHours;
        if (-1 == workHours) {
            return 0;
        }
        if (workHours <= 0) {
            std::cerr << "Error 1: Hours work cannot be negative." << std::endl;
            return 1;
        }
        double hourlyRate;
        std::cout << "Enter hourly rate of the worker ($00.00): ";
        std::cin >> hourlyRate;
        if (hourlyRate < 0) {
            std::cerr << "Error 2: Hourly rate cannot be negative." << std::endl;
            return 2;
        }
        double salary = static_cast<double>(workHours) * hourlyRate;
        if (workHours > 40) {
            salary += 0.5 * hourlyRate * (workHours - 40);
        }
        std::cout << "Salary is $" << std::setprecision(2) << std::fixed << salary << std::endl;
    }
    return 0;
}

