#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date1(10, 10, 2010);
    int month1;
    std::cin >> month1;
    date1.setMonth(month1);
    std::cout << "Your date is ";
    date1.displayDate();
    return 0;
} 

