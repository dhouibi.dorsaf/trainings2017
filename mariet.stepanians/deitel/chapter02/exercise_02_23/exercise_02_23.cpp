#include <iostream>

int
main()
{
    int number1;
    int number2;
    int number3;
    int number4;
    int number5;
    
    std::cout << "Enter five digits: ";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;

    /// smallest

    int min = number1;
    int max = number1;
    
    if (min > number2) {
        min = number2;
    }

    if (min > number3) {
        min = number3;
    }

    if (min > number4) {
        min = number4;
    }

    if (min > number5) {
        min = number5;
    }

    /// largest

    if (max < number2) {
        max = number2;
    }

    if (max < number3) {
        max = number3;
    }

    if (max < number4) {
        max = number4;
    }

    if (max < number5) {
        max = number5;
    }

    if (min == max) {
        std::cout << "The largest and smallest are equal: " << max << std::endl;
        return 0;
    } 
    
    std::cout << "The largest number: "  << max << std::endl;
    std::cout << "The smallest number: " << min << std::endl;
    
    return 0;
}
