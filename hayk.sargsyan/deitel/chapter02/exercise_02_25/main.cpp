#include <iostream>

int
main()
{
    int number1, number2;

    std::cout << "Enter the first number: ";
    std::cin >> number1;
    
    std::cout << "Enter the second number: ";
    std::cin >> number2;

    if (0 == number2) {
        std::cout << "ZeroError: integer modulo by zero\n";
        return 0;
    } 
    if (0 == number1 % number2) {
        std::cout << number1 << " is multiple of " << number2 << "\n";
    } 
    if (number1 % number2 != 0) { 
        std::cout << number1 << " isn't multiple of " << number2 << "\n";
    }

    return 0;
}
