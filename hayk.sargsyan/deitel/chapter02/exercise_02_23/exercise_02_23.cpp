#include <iostream>

int
main()
{
    int number1, number2, number3, number4, number5, theHighestNumber, theLowestNumber;

    std::cout << "Please enter first number: ";
    std::cin >> number1;
    std::cout << "Please enter second number: ";
    std::cin >> number2;
    std::cout << "Please enter third number: ";
    std::cin >> number3;
    std::cout << "Please enter forth number: ";
    std::cin >> number4;
    std::cout << "Please enter fifth number: ";
    std::cin >> number5;

    theLowestNumber = number1;
    theHighestNumber = number1;

    if (theLowestNumber > number2) {
        theLowestNumber = number2;
    }  
    if (theLowestNumber > number3) {
        theLowestNumber = number3;
    } 
    if (theLowestNumber > number4) {
        theLowestNumber = number4;
    } 
    if (theLowestNumber > number5) {
        theLowestNumber = number5;
    } 

    if (theHighestNumber < number2) {
        theHighestNumber = number2;
    }  
    if (theHighestNumber < number3) {
        theHighestNumber = number3;
    } 
    if (theHighestNumber < number4) {
        theHighestNumber = number4;
    } 
    if (theHighestNumber < number5) {
        theHighestNumber = number5;
    } 

    std::cout << "The lowest number is " << theLowestNumber << "\n"; 
    std::cout << "The highest number is " << theHighestNumber << "\n";

    return 0;
}
