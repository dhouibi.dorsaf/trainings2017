#include <string>

class MiniComputer
{
public:
    MiniComputer(std::string name);
    int run();
    std::string getComputerName();
    void setComputerName(std::string name);
private:
    void showMainMenu();
    int getMainCommand();
    int executeCommand(int command);
    int executeLoadCommand();
    int loadRegister(std::string register);
    int executeStoreCommand();
    int executeAddCommand();
    int executePrintCommand();
private:
    std::string computerName_;
    int a_;
    int b_;
    int c_;
    int d_;
};
