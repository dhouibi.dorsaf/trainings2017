#include <iostream>

int
main()
{
    double totalDistance = 0, totalGas = 0;

    while (true) {
        int distance;
        std::cout << "Enter miles driven (-1 to quit): ";
        std::cin >> distance;
        if (-1 == distance) {
            return 0;
        }
        if (distance < 0) {
            std::cout << "Error 1. Distance should be positive." << std::endl;
            return 1;
        }

        int gas;
        std::cout << "Enter gallon used: ";
        std::cin >> gas;
        if (gas < 0) {
            std::cout << "Error 2. Gallon should be positive." << std::endl;
            return 2;
        }

        double milePerGas = static_cast<double>(distance) / gas;
        std::cout << "MPG this tankful: " << milePerGas << std::endl;

        totalDistance += distance;
        totalGas += gas;
        double totalMilePerGas = totalDistance / totalGas;
        std::cout << "Total MPG: " << totalMilePerGas << "\n" << std::endl;
    }

    return 0;
}

